# Carl Zeiss Summerschool 2023

Install NIFTy:

```
pip install nifty8
```

alternatively with

```
git clone -b NIFTy_8 https://gitlab.mpcdf.mpg.de/ift/nifty.git
cd nifty
pip3 install .

```
# More resources

Documentation:
https://ift.pages.mpcdf.de/nifty/

Gitlab:
https://gitlab.mpcdf.mpg.de/ift/nifty
